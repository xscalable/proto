module gitlab.com/xscalable/proto

require (
	github.com/gogo/protobuf v1.2.0
	github.com/golang/protobuf v1.2.0
	gitlab.com/xscalable/types v0.0.0-20190115215518-5a25e46841b3
	google.golang.org/grpc v1.18.0
)
